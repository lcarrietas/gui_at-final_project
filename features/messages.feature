Feature: Observer messages options


    Send mesages to observee and teacher
    @send_message
    Scenario: Send mesagge to an observee
    Given Exist the "QA_{{random_string}}" course
      And There is a student enrolled in the course
      And The student pair code is provided
      And The student is added as observee
      And The user is logged in as Observer
     When Navigate to Inbox menu
     And Send a message to the observee
          |         Subject              |   Body    |
          | QA_Subject_{{random_string}} | Test Body |
     Then the message should appear in sent messages
