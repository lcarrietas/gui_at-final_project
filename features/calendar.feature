Feature: Observer calendar options

    Options related to the calendar, as create and delete events
    @Create_calendar_event
    Scenario: Create a calendar event
        Given The user is logged in as Observer
         When Navigate to calendar menu
          And Create an event in the calendar
              | Day   |     Title             |
              | today | AT_{{random_string}}  |
         Then The created event should be visible in the calendar

    @Delete_calendar_event
    Scenario: Delete a calendar event
        Given The user is logged in as Observer
          And A calendar event is created
              | Day   |     Title             | 
              | today | AT_{{random_string}}  |
         When Navigate to calendar menu
          And Delete the created calendar event
         Then The calendar event should be deleted
         