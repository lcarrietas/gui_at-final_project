Feature: Observe students

    Pair with an observee student
    @Pair_observee
    Scenario: Pair with an existing student as observer
        Given Exist the "QA_{{random_string}}" course
          And There is a student enrolled in the course
          And The student pair code is provided
          And The user is logged in as Observer
          When Navigate to account menu
          And Navigate to Observing and pair with a student
          Then The student name should be visible in the Observing panel
