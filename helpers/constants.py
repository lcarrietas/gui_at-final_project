from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.safari.options import Options as SafariOptions


class WebBrowserType:
    CHROME = 'chrome'
    FIREFOX = 'firefox'


browsers = {'chrome': ChromeOptions(), 'firefox': FirefoxOptions(), 'safari': SafariOptions()}
