from behave import given, when, then
from behave import use_step_matcher
import requests
from helpers.context_helper import replace_keywords
from pages.home_page import HomePage
from pages.account_page import AccountPage
from helpers.logger import Logger

use_step_matcher('re')


@given('Exist the "(?P<name>.*?)" course')
def create_course_by_api(context, name):
    # create request params and save useful data
    course_name = replace_keywords(name)
    context.course_name = course_name
    context.params['course[name]'] = course_name
    context.params['offer'] = "true"
    context.params['enroll_me'] = "true"

    url = f'{context.restapi_base_url}/accounts/{context.api_code_id}/courses'
    response = requests.request("POST", url, headers=context.headers['teacher'],
                                params=context.params)

    context.course_id = response.json()["id"]
    context.course_actual_name = response.json()["name"]

    if (context.course_actual_name == context.course_name):
        LOGGER = Logger('Given')
        LOGGER.info(f'The course {context.course_name} was created')
        LOGGER.close()

    assert context.course_actual_name == context.course_name


@given('There is a student enrolled in the course')
def enroll_student(context):
    # set the request params
    context.params['enrollment[user_id]'] = context.api_student_id
    context.params['enrollment[type]'] = 'StudentEnrollment'

    url = f'{context.restapi_base_url}/courses/{context.course_id}/enrollments'
    response = requests.request("POST", url, headers=context.headers['teacher'],
                                params=context.params)

    user_id = response.json()['user_id']
    context.enroll_id = response.json()['id']

    assert str(user_id) == context.api_student_id


@given('The student pair code is provided')
def provide_pair_code(context):
    # accept enrollment invitation
    url_accept = ('{0}/courses/{1}/enrollments/{2}/accept'
                  .format(context.restapi_base_url, context.course_id, context.enroll_id))

    response_accept = requests.request("POST", url_accept, headers=context.headers['student'],)
    enrollment = response_accept.json()['success']

    # provide pair code
    url_code = f'{context.restapi_base_url}/users/{context.api_student_id}\
                  /observer_pairing_codes'
    response_pairing = requests.request("POST", url_code, headers=context.headers['student'])
    pairing_code = response_pairing.json()['code']
    context.pairing_code = pairing_code
    student_id = response_pairing.json()['user_id']

    if enrollment:
        assert str(student_id) == context.api_student_id
    else:
        inner_logger = Logger('Given')
        title_msg = f'Enrollment {context.enroll_id}Failed'
        inner_logger.error(title_msg)
        assert False


@when('Navigate to account menu')
def navigate_to_menu(context):
    home_page = HomePage(context.webdriver)
    home_page.navigate_to_account()


@when('Navigate to Observing and pair with a student')
def pair_observee(context):
    account_page = AccountPage(context.webdriver)
    account_page.pair_observee(context.pairing_code)


@then('The student name should be visible in the Observing panel')
def verify_pairing(context):
    account_page = AccountPage(context.webdriver)
    observee = account_page.find_observee(context.student_name)
    student_name = context.student_name
    assert student_name in observee
