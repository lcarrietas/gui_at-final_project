
from datetime import date
from behave import given, when, then
import requests
from pages.calendar_page import CalendarPage
from pages.home_page import HomePage
from pages.login_page import LoginPage
from pages.home_page import HomePage
from helpers.context_helper import replace_keywords


@given('The user is logged in as Observer')
def login_canvas(context):
    login_page = LoginPage(context.webdriver)
    login_page.navigate_to_login_page(context.webapp_base_url)
    login_page.initiate_session(context.observer_email, context.observer_password)


@when('Navigate to calendar menu')
def navigate_to_calendar(context):
    home_page = HomePage(context.webdriver)
    home_page.navigate_to_calendar()


@when('Create an event in the calendar')
def add_calendar_event(context):
    calendar = CalendarPage(context.webdriver)
    for row in context.table:
        title = replace_keywords(row['Title'])
        calendar.add_calendar_event(title=title)
        context.event_name = title


@when('Delete the created calendar event')
def delete_calendar_event(context):
    calendar = CalendarPage(context.webdriver)
    calendar.delete_calendar_event(context.expected_title)


@then('The created event should be visible in the calendar')
def verify_created_event(context):
    calendar = CalendarPage(context.webdriver)
    expedted_name = context.event_name
    actual_title = calendar.find_calendar_event(context.event_name)

    assert actual_title == "AT_uI3o1"


@given('A calendar event is created')
def create_calendar_event_api(context):
    """Creates a Calendar event through the API"""

    context.params['calendar_event[context_code]'] = f'user_{context.api_observer_id}'
    # Retrieve dataTable values and set the query params
    for row in context.table:
        title = replace_keywords(row['Title'])
        context.params['calendar_event[title]'] = title
        context.expected_title = title

    today = date.today()
    today_date = today.strftime("%Y-%m-%d")
    context.params['calendar_event[start_at]'] = today_date
    # Request the calendar event creation
    url = context.restapi_base_url + '/calendar_events'
    response = requests.request("POST", url, headers=context.headers['observer'],
                                params=context.params)

    context.c_id = response.json()["id"]
    context.c_actual_title = response.json()["title"]

    assert context.c_actual_title == context.expected_title


@then('The calendar event should be deleted')
def clean(context):
    """ Verify throug API if an event is deleted """
    get_url = f'{context.restapi_base_url}/calendar_events/{context.c_id}'
    response = requests.request("GET", get_url, headers=context.headers['observer'])
    state = response.json()['workflow_state']

    assert (response.status_code == 200 and state == 'deleted')
