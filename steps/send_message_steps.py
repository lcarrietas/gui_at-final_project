import requests
from behave import given, when, then
from behave import use_step_matcher
from pages.home_page import HomePage
from pages.messages_page import MessagesPage
from helpers.context_helper import replace_keywords


use_step_matcher('re')


@given('The student is added as observee')
def add_observee_api(context):
    # Send the request to add an observee
    context.params['access_token'] = context.api_student_token
    id = context.api_observer_id
    url = f'https://canvas.instructure.com/api/v1/users/{id}/observees'
    response = requests.request("POST", url, headers=context.headers['observer'],
                                params=context.params)
    # save useful response information
    expected_student_id = context.api_student_id
    actual_student_id = response.json()['id']
    code = response.status_code
    context.student_name = response.json()['short_name']

    assert expected_student_id == str(actual_student_id) and code == 200


@when('Navigate to Inbox menu')
def navigate_to_menu(context):
    home_page = HomePage(context.webdriver)
    home_page.navigate_to_inbox()


@when('Send a message to the observee')
def send_message(context):
    course = context.course_name
    student = context.student_name
    message_page = MessagesPage(context.webdriver)
    # Get the datatable values to send the message
    for row in context.table:
        subject = replace_keywords(row['Subject'])
        context.subject = subject
        body = row['Body']
        context.body = body
        message_page.send_message(student, course, subject, body)


@then('the message should appear in sent messages')
def verify_sent_message(context):
    message_page = MessagesPage(context.webdriver)
    actual_subject = message_page.find_sent_message(context.subject)

    assert context.subject in actual_subject
