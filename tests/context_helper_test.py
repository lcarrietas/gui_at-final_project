from helpers import context_helper as ch
import random
import re
import string

regex = '{{(.*?)}}'
default_length = 10


class TestObject:
    question = "why so serious"


def test_replace_keywords():
    text = ch.replace_keywords("{{random_string}}")
    assert len(text) == 5


def test_replace_keywords_negative():
    text = ch.replace_keywords("{{random string}}")
    assert text == "{{random string}}"


def test_replace_keywords_negative_no_curly():
    text = ch.replace_keywords("random_string")
    assert text == "random_string"


def test_get_random_string():
    assert len(ch.get_random_string(10)) == 10


def test_replace_variables():
    object_step = TestObject()
    text = ch.replace_variables("Test {{question}}", object_step)
    assert text == "Test why so serious"
