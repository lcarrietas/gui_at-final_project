from allure_commons.types import AttachmentType
from helpers.selenium.webdriver_manager import WebDriverManager
import allure
import requests
from helpers.logger import Logger


def before_all(context):
    context.browser = context.config.userdata['browser']
    context.timeout_in_seconds = context.config.userdata['timeout_in_seconds']

    context.remote_url = context.config.userdata['remote_url']
    context.remote_provider = context.config.userdata['remote_provider']
    context.execution_type = context.config.userdata['execution_type']

    context.webapp_base_url = context.config.userdata['webapp_base_url']
    context.observer_email = context.config.userdata['webapp_observer_email']
    context.observer_password = context.config.userdata['webapp_observer_password']

    context.restapi_base_url = context.config.userdata['restapi_base_url']
    context.api_observer_token = context.config.userdata['observer_token']
    context.api_observer_id = context.config.userdata['observer_id']
    context.api_teacher_token = context.config.userdata['teacher_token']
    context.api_code_id = context.config.userdata['code_id']
    context.api_student_token = context.config.userdata['student_token']
    context.api_student_id = context.config.userdata['student_id']
    context.student_name = "Premier Student "

    context.headers = {'observer': {'Authorization': 'Bearer ' + context.api_observer_token},
                       'teacher': {'Authorization': 'Bearer ' + context.api_teacher_token},
                       'student': {'Authorization': 'Bearer ' + context.api_student_token}
                       }


def before_scenario(context, scenario):
    context.webdriver = WebDriverManager(context.browser,
                                         context.timeout_in_seconds,
                                         context.execution_type,
                                         context.remote_provider,
                                         context.remote_url)
    # context.webdriver.navigate_to(context.webapp_base_url)

    # log
    inner_logger = Logger('Scenario')
    title_msg = f'*** START Scenario: {scenario.name} ***'
    inner_logger.info(title_msg, is_title=True)
    inner_logger.close()


def after_scenario(context, scenario):
    context.webdriver.close()
    # log
    inner_logger = Logger('Scenario')
    title_msg = f'*** Scenario: {scenario.name} finished whit {scenario.status}***'
    inner_logger.info(title_msg, is_title=True)


def before_step(context, step):
    context.params = {}
    # log
    inner_logger = Logger('Step')
    title_msg = f'{step.name} Initiated '
    inner_logger.info(title_msg)


def after_step(context, step):
    if step.status == 'failed':
        allure.attach(context.webdriver.get_screenshot_as_png(),
                      'screenshot', AttachmentType.PNG)
    inner_logger = Logger('Step')
    title_msg = f'{step.name} finished whit {step.status}'
    inner_logger.info(title_msg)


def after_all(context):
    inner_logger = Logger('Cleaner')
    context.params = {}
    # Get Canvas Courses
    get_url = context.restapi_base_url + '/courses'
    response = requests.request("GET", get_url, headers=context.headers['teacher'])
    for course in response.json():
        # Delete Courses created by AT
        if "QA_" in course['name']:
            name = course['name']
            course_id = course['id']
            del_url = context.restapi_base_url + '/courses/' + str(course_id)
            context.params['event'] = 'delete'
            response_del = requests.request("DELETE", del_url, headers=context.headers['teacher'],
                                            params=context.params)
            # Log
            if response_del.status_code == 200:
                title_msg = f'* Resource: {name} successfully cleaned'
                inner_logger.info(title_msg)

            else:
                title_msg = f'Delete course failed, status code: {response_del.status_code}'
                inner_logger.error(title_msg)
