pipeline {

	agent { label 'built-in' }


    parameters {
        choice(choices: 'chrome\nfirefox', description: 'Choose a web browser', name: 'BROWSER')
        choice(choices: 'selenium_grid\nsauce_labs', description: 'Choose a remote provider to run the tests', name: 'REMOTE_PROVIDER')
        choice(choices: 'Create_calendar_event\nDelete_calendar_event\nSend_message\nPair_observee', description: 'Choose the automated test to run', name: 'SCENARIO')
    }

    environment {
        // This is always remote
        EXECUTION_TYPE = 'remote'
        SAUCE_ACCESS_KEY=credentials('SAUCE_ACCESS_KEY')
        SAUCE_USERNAME=credentials('SAUCE_USERNAME')
    }

    stages {

        stage('GUI Automated Test') {

			agent {
				docker {
					image 'python:3.11-rc'
				}
			}

			stages {
                stage("Install requirements") {
                    steps {
                        sh '''
                            python -m venv .venv
                            . .venv/bin/activate
                            pip install -r requirements.txt
                        '''
                    }
                }

                stage ('Run unit tests') {
                    steps {
                        sh '''
                            . .venv/bin/activate
                            pytest -v
                        '''
                    }
                }

                stage ('Run GUI tests') {
                        // This can be configured based on the parameter REMOTE_PROVIDER
                    steps {
                        script {
                            if (REMOTE_PROVIDER == 'selenium_grid') {
                                sh '''
                                    . .venv/bin/activate
                                    mkdir logs/
                                    behave --tags=${SCENARIO} \
                                    -D browser=${BROWSER} \
                                    -D remote_provider=${REMOTE_PROVIDER} \
                                    -D execution_type=${EXECUTION_TYPE} \
                                    -D remote_url=http://192.168.33.61:4444 \
                                    -f allure_behave.formatter:AllureFormatter -o ./results
                                '''
                            }
                            else {
                                sh '''
                                    . .venv/bin/activate
                                    mkdir logs/
                                    export SAUCE_USERNAME=${SAUCE_USERNAME}
                                    export SAUCE_ACCESS_KEY=${SAUCE_ACCESS_KEY}
                                    behave --tags=${SCENARIO} \
                                    -D browser=${BROWSER} \
                                    -D remote_provider=${REMOTE_PROVIDER} \
                                    -D execution_type=${EXECUTION_TYPE} \
                                    -D remote_url=https://ondemand.us-west-1.saucelabs.com/wd/hub \
                                    -f allure_behave.formatter:AllureFormatter -o ./results
                                '''
                            }
                        }
                        stash name: 'results', includes: "results/*"
                    }
                }
            }
        }
    }

    post {
        always {
			unstash 'results'
			script {
				allure([
					includeProperties: false,
					jdk: '',
					properties: [],
					reportBuildPolicy: 'ALWAYS',
					results: [[path: 'results']]
				])
			}
        }
    }
}