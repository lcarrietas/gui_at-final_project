from pages.base_page import BasePage


class LoginPage(BasePage):
    """ Canvas.Instructure Login Page locators"""

    def navigate_to_login_page(self, base_url):
        self._webdriver.navigate_to(base_url+'/login/canvas')

    def initiate_session(self, email, password):
        email_textbox = self._webdriver.find_by_id('pseudonym_session_unique_id')
        email_textbox.send_keys(email)

        pass_textbox = self._webdriver.find_by_id('pseudonym_session_password')
        pass_textbox.send_keys(password)

        login_button = self._webdriver.find_by_xpath(
                    '//div[@class="ic-Form-control ic-Form-control--login"]\
                    /button[@class="Button Button--login"]')
        login_button.click()

    # def initiate_session_google(self, email, password):
    #     login_google_button = self._webdriver.find_by_css('a.ic-Login__sso-button--google')
    #     login_google_button.click()

    #     email_input = self._webdriver.find_by_id('identifierId')
    #     email_input.send_keys(email)

    #     next_button = self._webdriver.find_by_xpath('//button/span[text()="Siguiente"]')
    #     next_button.click()

    #     passwod_input = self._webdriver.find_by_xpath('//input[@type="password"]')
    #     passwod_input.send_keys(password)

    #     next_button = self._webdriver.find_by_xpath('//button/span[text()="Siguiente"]')
    #     next_button.click()
