from pages.base_page import BasePage
from selenium.webdriver.support.ui import Select
from time import sleep


class MessagesPage(BasePage):
    """Canvas.Instructure Messages Page locators"""

    def send_message(self, name, course, subject, body):
        # open the send message menu
        compose_button = self._webdriver.find_by_id('compose-btn')
        compose_button.click()
        # find select course option
        course_button = self._webdriver.find_by_xpath(
            '//button[@data-id="compose-message-course"]')
        course_button.click()
        # select favorite coursees
        select_course = self._webdriver.find_by_xpath(
            '//div[@id="compose-message-course-bs"]/ul/li/a[@role="button"]/span')
        select_course.click()
        # Search for the course where the observee is
        search_course_input = self._webdriver.find_by_xpath(
            '//ul[@style]/li/div/label/input[@placeholder="Favorite Courses"]')
        search_course_input.send_keys(course)
        # Select the desired course
        desired_course = self._webdriver.find_by_xpath(
            '//div[@data-content-type="Favorite Courses"]/ul/li[@aria-hidden="false"]')
        desired_course.click()
        # Open the address book
        address_book = self._webdriver.find_by_id('recipient-search-btn')
        address_book.click()
        # look for students
        students_item = self._webdriver.find_by_xpath(
            '//li[@aria-label="Students"]')
        students_item.click()
        # find the desired student to send the message
        desired_student = self._webdriver.find_by_xpath(
            f'//li[@aria-label="{name}"]')
        desired_student.click()
        # Set the message subject
        subject_input = self._webdriver.find_by_id('compose-message-subject')
        subject_input.send_keys(subject)
        # Set the message Body
        body_textarea = self._webdriver.find_by_css(
            'textarea.conversation_body')
        body_textarea.send_keys(body)
        # send message
        send_button = self._webdriver.find_by_xpath(
            '//div[@aria-hidden="false"]/div/div/button[@data-track-label="Send"]')
        send_button.click()
        sleep(1)

    def find_sent_message(self, name='', subject='', body=''):
        # select sent message option
        sent_option = Select(self._webdriver.find_by_id('conversation_filter_select'))
        sent_option.select_by_value('sent')
        # select the sent message
        subject = self._webdriver.find_by_xpath(
            f'//div/h3[contains(text(),"{subject}")]')

        return subject.text
