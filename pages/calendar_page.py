from pages.base_page import BasePage
from selenium.webdriver.common.action_chains import ActionChains
from time import sleep


class CalendarPage(BasePage):
    """ Canvas.Instructure Calendar Page locators """

    def add_calendar_event(self, title):
        calendar_day = self._webdriver.find_by_css(
            'div.calendar.fc.fc-ltr.fc-unthemed >div > div > table >tbody > tr > td > div >div \
             >div div >table > tbody >tr > td.fc-today')
        a = self._webdriver.execute_script("arguments[0].scrollIntoView();", calendar_day)
        calendar_day.click()

        title_input = self._webdriver.find_by_id('calendar_event_title')
        title_input.send_keys(title)

        submit_button = self._webdriver.find_by_css(
            'form#edit_calendar_event_form button.event_button.btn.btn-primary')
        submit_button.click()

    def delete_calendar_event(self, title):
        event = self._webdriver.find_by_xpath(f'//a[contains(@title, "{title}")]')
        event.click()

        delete_button = self._webdriver.find_by_css('button.Button--small.delete_event_link')
        delete_button.click()

        confir_delete_buttn = self._webdriver.find_by_xpath(
            '//div[contains(@style, "display: block")]/div/div/button/span[text()="Delete"]')
        confir_delete_buttn.click()

    def find_calendar_event(self, calendar_title):
        event_calendar = self._webdriver.find_by_xpath(
            f'//span[text()="AT_uI3o1"]')
        event_calendar.click()

        event_title = self._webdriver.find_by_xpath('//div[@class="event-details-header"]/h2/a')
        return event_title.text
