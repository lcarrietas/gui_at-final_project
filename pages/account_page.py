from pages.base_page import BasePage


class AccountPage(BasePage):
    """Canvas.Instructure Account Page locators"""

    def pair_observee(self, code):
        # go to observing option
        observees_anchor = self._webdriver.find_by_xpath(
            '//div/a[@href="/profile/observees"]')
        observees_anchor.click()
        # Set the pairing code
        pairing_code_input = self._webdriver.find_by_id('pairing_code')
        pairing_code_input.send_keys(code)
        # add the student as observee
        add_student_button = self._webdriver.find_by_css(
            'form.add-observee-form > button.btn-primary')
        add_student_button.click()

    def find_observee(self, name):
        observee_link = self._webdriver.find_by_xpath(
            f'//div[@class="observees-list-container"]/ul/li[text()="{name}"]')
        observee = observee_link.text
        return observee
