from pages.base_page import BasePage


class HomePage(BasePage):
    """Canvas.Instructure Home Page locators"""

    def navigate_to_calendar(self):
        calendar_anchor = self._webdriver.find_by_css('#global_nav_calendar_link')
        calendar_anchor.click()

    def navigate_to_account(self):
        account_button = self._webdriver.find_by_id('global_nav_profile_link')
        account_button.click()

    def navigate_to_inbox(self):
        inbox_anchor = self._webdriver.find_by_id('global_nav_conversations_link')
        inbox_anchor.click()
